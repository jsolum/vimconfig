" Neovim Config
" James Solum

""""""""""""""" Basics """"""""""""""""""
let mapleader = " "

set mouse=a                        " use a mouse if available
set termguicolors                  " enable true colors support 
set number                         " line numbers
set background=dark
set scrolloff=7                    " set number of context lines when scrolling
set ignorecase                     " set default search to be case insensitve
set smartcase                      " default case search unless there are upper case
set hlsearch                       " highlights your search
set hidden                         " so we can switch buffers w/out needing to save them
set noswapfile                     " remove swap files
set autoindent                     " copy the indentation from the previous line
set smartindent                    " Changes indent based on file extension
set nowrap                         " don't wrap lines
set backspace=indent,eol,start     " not sure what this is :(
set clipboard^=unnamed,unnamedplus " Use system clipboard for yank register
set showmatch                      " highlight search match as you type
set splitbelow                     " default horizontal split below
set splitright                     " default vertical split right

                               " Tabs/Spaces
set softtabstop=2              " tab = 2 spaces
set tabstop=2                  " set the default tabstop
set expandtab                  " turn tabs into spaces

syntax on

" Moving to different buffers with arrows
nmap <Left> :bprev<CR>
nmap <Right> :bnext<CR>

""""""""""""""" Plugins """"""""""""""""""
" Plugin Manager: https://github.com/junegunn/vim-plug
" Auto Install Plugin Manager
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync 
endif

call plug#begin('~/.config/nvim/plugged')

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }   " Ensure we have the up to date binary
Plug 'junegunn/fzf.vim'                               " Fuzzy Finder: https://github.com/junegunn/fzf
Plug 'joshdick/onedark.vim'                           " Colorscheme
Plug 'scrooloose/nerdtree'                            " Side menu for files
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'Yggdroot/indentLine'                            " shows indentation level lines
Plug 'editorconfig/editorconfig-vim'                  " Use editor config preferences
Plug 'kshenoy/vim-signature'                          " Shows marks on the side
Plug 'sheerun/vim-polyglot'                           " better syntax highlighting
Plug 'scrooloose/nerdcommenter'                       " Easy auto commenting
Plug 'qpkorr/vim-bufkill'                             " Remove buffers w/out removing splits

" Rails
Plug 'tpope/vim-rails' " Ruby Shortcuts

" Language Server
Plug 'neoclide/coc.nvim', {'branch': 'release'}       " Code Completion!
" CocInstall
"     coc-solargraph  Ruby language support
"     coc-java        Java languages Support


call plug#end()

"""""""""""""" Theme """""""""""""
colorscheme onedark

""""""""""""" FZF Config """"""""""""""""
"FZF remapping:
"    ctrl-g will let you search your current git repo files
"    ctrl-p will let you search all your files
map <leader>p :GFiles<CR>

" Fuzzy Finder
noremap <leader>f :Files <CR>

"map <Leader>l :Lines<CR>
"map <Leader>b :Buffer<CR>
"map <Leader>m :Marks<CR>
"map <Leader>y :BTags<CR>
"map <Leader>Y :Tags<CR>
"map <Leader>; :History:<CR>
"map <Leader>/ :History/<CR>
"map <Leader>r :registers<CR>

""""""""""""" Nerd Tree """"""""""""""""
" Toggle Nerd tree
map <leader>n :NERDTreeToggle <CR> 

" Open current file in nerd tree
nnoremap <silent> <Leader>m :NERDTreeFind<CR>

let NERDTreeShowHidden=1           " show hidden files in nerdtree
let NERDTreeQuitOnOpen = 1         " quit nerdtree when opening a file
let NERDTreeAutoDeleteBuffer = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1

""""""""""""" Airline  """"""""""""""""
let g:airline_theme='onedark'
set laststatus=2

let g:airline#extensions#tabline#enabled = 1

""""""""""""" COC  """"""""""""""""
" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
" <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
if exists('*complete_info')
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}
